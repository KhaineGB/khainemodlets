Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish
ammo44MagnumBulletSteel,items,Ammo,New,".44 Magnum Round, Steel (Ammo)",,,,,
ammo9mmBulletSteel,items,Ammo,New,"9mm Round, Steel (Ammo)",,,,,
ammo762mmBulletFMJSteel,items,Ammo,New,"7.62mm Round, Steel (Ammo)",,,,,
ammoSteelCasedGroupDesc,items,Ammo,New,Steel-cased ammunition is a cheap alternative to using brass but wears down barrels faster.,,,,,
resourceBulletCasingSteel,items,Item,New,"Bullet Casing, Steel",,,,,
resourceBulletCasingSteelDesc,items,Item,New,Steel bullet casings are cheaper but cause more wear on the barrel.,,,,,